#ifndef FUNCTION_H
#define FUNCTION_H

#include <cmath>

double eshka(double x, double e);

double sin(double x, double e);

double cos(double x, double e);

double ln(double x, double e);

double deg(double x, double e, double a);

double arctg(double x, double e);

#endif // FUNCTION_H

#include "test.h"

void test::test_deg()
{
    double e = 0.0001;
    double x = -0.4;
    double a = 2;
    QCOMPARE(0.36, deg(x, e, a));

    double e1 = 0.1;
    double x1 = 0.5;
    double a1 = 2;
    QCOMPARE(2.25, deg(x1, e1, a1));
}

#include "function.h"

double sin(double x, double e)
{

    double count = 3;
    double res = x;
    double znak = -1;
    double xxx = x*x*x;
    double fuc = 6;
    do
    {
        res = res + znak*xxx/fuc;
        count = count+2;
        xxx = xxx*x*x;
        fuc=fuc*(count-1)*count;
        znak = znak*(-1);
    }while(fabs(xxx/fuc) >= e);
    return res;
}

#include "function.h"

double cos(double x, double e)
{
    double count = 2;
    double res = 1;
    double sign = -1;
    double variable = x*x;
    double fuc = 2;
    do
    {
        res = res + sign*variable/fuc;
        count = count+2;
        variable = variable*x*x;
        fuc=fuc*(count-1)*count;
        sign = sign*(-1);
    }while(fabs(variable/fuc) >= e);
    return res;
}
